import React, { Component } from 'react';
import FormSearchHotel from "./Form";
import Center from 'react-center';
class App extends Component {

    constructor() {
      super();
      this.state = {
        open:false,
        openErro: false
      };
    };

    handleToggle(event, toggled){
        this.setState({
            [event.target.name]: toggled,
        });
    };
    handleClose(e){
        this.setState({open: false, openErro: false});
    };
    render() {
        return (
              <FormSearchHotel></FormSearchHotel>
        );
    }
}
export default App;
