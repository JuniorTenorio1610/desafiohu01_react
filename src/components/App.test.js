jest.dontMock('react-bootstrap');
jest.dontMock('classnames');
jest.mock('./App', () => 'App');
jest.mock('./Form', () => 'FormSearchHotel');
import React from 'react';
import FormSearchHotel from "./Form";
import App from './App';
import renderer from 'react-test-renderer';

test('Check component App', () => {
  const component = renderer.create(
    <App>
    </App>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Check component Form', () => {
  const component = renderer.create(
    <FormSearchHotel>
    </FormSearchHotel>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toEqual({"children": null, "props": {}, "type": "FormSearchHotel"}
);
});
