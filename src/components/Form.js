import React from "react";
import {FormGroup, ControlLabel, ListGroup, Checkbox, Form, Button, Grid, Row, Col, Modal, ListGroupItem} from 'react-bootstrap';
import {Typeahead} from 'react-bootstrap-typeahead';
import fetch from "isomorphic-fetch";
import Center from 'react-center';
//import {DatePicker} from 'react-bootstrap-date-picker';
var DatePicker = require("react-bootstrap-date-picker");
var hotelReserv = [];
const inputsForm = {
  width : '100%'
}
const divTitle = {
  background : '#00b5d4',
  color : '#fff',
  fontSize : '30px'
}
const divSubTitle = {
  background : '#fff',
  color : '#00b5d4',
  fontSize : '25px'
}
const divForm = {
  background : '#fff',
  color : '#000',
  fontSize : '18px'
}
const checkBox = {
  background : '#fff',
  color : '#000',
  fontSize : '14px'
}
var FormSearchHotel = React.createClass({

  getInitialState() {
    return {
      value: '',
      text: [],
      minDate: new Date().toISOString(),
      maxDate: new Date().toISOString(),
      autoOk: false,
      disableYearSelection: false,
      idHotel:'',
      hotel:'',
      noDate: '0',
      disp: [],
      reserv: [],
      dispHotel: '',
      open: false,
      openErro: false,
    };
  },

  componentDidMount() {
    var self = this;
    fetch('http://localhost:9000/hoteis')
    .then(function(response) {
        if (response.status >= 400) {
            throw new Error("Bad response from server");
        }
        return response.json();
    })
    .then(function(data) {
        self.setState({text:data[0].hotel});
        self.setState({disp:data[1].disp});
    }).catch((error) => {
        console.error(error);
    });
  },

  getValidationState() {
    const length = this.state.value.length;
    if (length > 10) return 'success';
    else if (length > 5) return 'warning';
    else if (length > 0) return 'error';
  },

  handleChange(e) {
    this.setState({ value: e.target.value });
  },

  handleUpdateInput(value) {
    if (value.length) {
      this.state.idHotel = value[0].key;
      this.state.hotel = value[0].value;
      this._searchHotel();
    }
  },

  _searchHotel(){
    var count = 0;
    var hotel = [];
    var i = 0;
    var len = 0;
    if(this.state.idHotel === 'city'){
        for (len = this.state.text.length; i < len; i++) {
            if(this.state.text[i].city === this.state.hotel){
                hotel[count] = this.state.text[i];
                count++;
            }
        }
    } else {
        for (len = this.state.text.length; i < len; i++) {
            if(this.state.text[i].key === this.state.idHotel){
                hotel[count] = this.state.text[i];
                count++;
            }
        }
    }

    this.state.dispHotel = hotel;
  },
  handleChangeMinDate(value, formattedValue){
    this.setState({
      minDate: value,
      formattedValue: formattedValue
    });
  },

  handleChangeMaxDate(value, formattedValue){
    this.setState({
      maxDate: value,
      formattedValue: formattedValue
    });
  },
  handleChangeNoDate(value){
    this.state.noDate = '1'
  },

  handleSubmit(event) {
    if(this.state.noDate === '0'){
        if(this.state.minDate === undefined || this.state.maxDate === undefined){
            this.setState({openErro: true});
        } else if(this.state.minDate > this.state.maxDate){
            this.setState({openErro: true});
        } else {
            this._processHotel(this);
            this.setState({open: true});
        }
    }else{
        this._processHotel(this);
        this.setState({open: true});
    }
  },
  _processHotel(e){
    hotelReserv = [];
    var i = 0;
    var len = 0;
    var x = 0;
    var leng = 0;
    if(this.state.noDate === '0'){
        for (len = this.state.dispHotel.length; i < len; i++) {
          x = 0;
            for(leng = this.state.disp.length; x < leng; x++){
                if(this.state.dispHotel[i].key  === this.state.disp[x].hotel){
                    var days = parseInt(this.state.disp[x].min, 10);
                    var startDate = '';
                    //var startDateMin = this.state.minDate;
                    var startDateMin = new Date(this.state.minDate);
                    var endDate = new Date(this.state.maxDate);
                    var dt = new Date(this.state.disp[x].data.split("/").reverse().join("-"));
                    startDateMin = startDateMin.getDate()+"/"+ startDateMin.getMonth() + "/" + startDateMin.getFullYear();
                    startDate = dt.getDate()+"/"+ dt.getMonth() +"/" + dt.getFullYear()
                    dt.setDate(dt.getDate() + days)
                    if(startDateMin === startDate && dt <= endDate){
                      hotelReserv.push(<ListGroupItem key={x} header={"Hotel: " + this.state.dispHotel[i].text}> {"Data disponível: " + this.state.disp[x].data + " Cidade: " + this.state.dispHotel[i].city}</ListGroupItem>);
                    }
                }
            }
        }
    } else {
        for (len = this.state.dispHotel.length; i < len; i++) {
          x = 0;
            for(leng = this.state.disp.length; x < leng; x++){
                if(this.state.dispHotel[i].key  === this.state.disp[x].hotel){
                        hotelReserv.push(<ListGroupItem key={x} header={"Hotel: " + this.state.dispHotel[i].text}> {"Data disponível: " + this.state.disp[x].data + " Cidade: " + this.state.dispHotel[i].city}</ListGroupItem>);
                }
            }
        }
    }
  },
  handleClose() {
    this.setState({ open: false });
  },

  handleOpen() {
    this.setState({ open: true });
  },
  render() {
    return (
      <Grid>
        <Row>
          <Col xs={2} md={1}>
          </Col>
          <Col xs={14} md={10} style={divTitle}>
          <Grid>
            <Row>
              <Col xs={6} md={4} style={divTitle}>&nbsp;</Col>
              <Col xs={6} md={4} style={divTitle}>Hotéis</Col>
              <Col xs={6} md={4}></Col>
            </Row>
          </Grid>
          <Row style={divSubTitle}>
            <Col xs={1} md={1}></Col>
            <Col xs={16} md={10}><Center>+ 170.000 hotéis, pousadas e resorts no mundo todo.</Center><br/></Col>
            <Col xs={1} md={1}></Col>
          </Row>
          <Row style={divForm}>
          <Col xs={6} md={4} style={divForm}><ControlLabel>Quer ficar aonde?</ControlLabel></Col>
          <Col xs={6} md={4} style={divForm}><ControlLabel>Quando?</ControlLabel>(Entrada e saida)</Col>
          <Col xs={6} md={4} style={divForm}><ControlLabel></ControlLabel></Col>
          <br/>
          </Row>
          <Row style={divForm}>
            <Form inline>
            <Col xs={6} md={4} style={divForm}>
              <FormGroup
                controlId="formBasicText"
                validationState={this.getValidationState()}
              >
                <Typeahead
                  onChange={this.handleUpdateInput}
                  placeholder="cidade ou hotel  "
                  options={this.state.text}
                  labelKey={'text'}
                  style={inputsForm}
                />
                </FormGroup>
                </Col>
                <Col xs={6} md={4} style={divForm}>
                <FormGroup
                  controlId="formBasicText"
                  validationState={this.getValidationState()}
                >
                <DatePicker
                  id="example-datepicker"
                  value={this.state.minDate}
                  onChange={this.handleChangeMinDate}
                  dateFormat={"DD/MM/YYYY"}
                  style={inputsForm}
                />
                </FormGroup>
                </Col>
                <Col xs={6} md={4} style={divForm}>
                <FormGroup
                  controlId="formBasicText"
                  validationState={this.getValidationState()}
                >
                <DatePicker
                  id="example-datepicker"
                  value={this.state.maxDate}
                  onChange={this.handleChangeMaxDate}
                  dateFormat={"DD/MM/YYYY"}
                  style={inputsForm}
                />
                </FormGroup>
                </Col>
            </Form>
            </Row>
            <Row style={checkBox}>
            <Col xs={6} md={4}></Col>
            <Col xs={6} md={4}></Col>
            <Col xs={6} md={4}>
            <FormGroup>
              <Checkbox onChange={this.handleChangeNoDate}>
                Ainda não defini as datas
              </Checkbox>
            </FormGroup>
            </Col>
            </Row>
            <Row style={divForm}>
            <Col xs={6} md={4}></Col>
            <Col xs={6} md={4}>
            <Center>
            <Button
              bsStyle="primary"
              onClick={this.handleSubmit}>
              Buscar
            </Button>
            </Center>
            </Col>
            <Col xs={6} md={4}></Col>
            </Row>
          </Col>
          <Col xs={2} md={1} style={{background:'#fff'}}></Col>
        </Row><Modal show={this.state.open} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Hotel Disponiveis</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <ListGroup>
              {hotelReserv}
            </ListGroup>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.handleClose}>Close</Button>
            </Modal.Footer>
          </Modal>
      </Grid>
    );
  }
});

module.exports = FormSearchHotel;
