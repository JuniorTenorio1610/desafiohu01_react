# Desafio HU

### Installation

Clone this repository:

### `git clone git@bitbucket.org:JuniorTenorio1610/desafiohu01_react.git`

Enter project folder install dependencies:

### `cd desafiohu01_react && npm install`

### Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode and express API.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
