const express = require('express');
const morgan = require('morgan');
const path = require('path');
const fs = require('fs');
const async = require('async');
const app = express();

var hoteis = fs.readFileSync('database/hoteis.txt').toString().split("\n");
var disponibilidade = fs.readFileSync('database/disp.txt').toString().split("\n");
var city = hoteis;

async.series([
  function(callback) {
    objDisp = [];
    arrayDisp = disponibilidade;
    arrayDisp.forEach(function(data){
      objTemp = data.toString().split(",");
      objDisp.push({hotel:objTemp[0], data:objTemp[1], disp:objTemp[2], min:objTemp[3]});
      disponibilidade = objDisp;
    })
    return callback;
  }
]);

async.series([
  function(callback){
    objHotel = [];
    arrayHotel = hoteis;

    arrayHotel.forEach(function(data){
      objTemp = data.toString().split(",");
      objHotel.push({key:objTemp[0], text:objTemp[2], value:objTemp[0], city:objTemp[1]});
      hoteis = objHotel;
    })
    return callback;
  }
]);

async.series([
  function(callback){
    objCity = [];
    arrayCity = [];
    key = 0
    arrayHotel = city;
    arrayHotel.forEach(function(data){
      objTemp = data.toString().split(",");
      if(arrayCity[objTemp[1]] == undefined) {
        arrayCity[objTemp[1]] = objTemp[1];
        hoteis.push({key:'city', text:objTemp[1], value:objTemp[1], city:''});
      }
    })
    return callback;
  }
]);

// Setup logger
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));
// Serve static assets
app.use(express.static(path.resolve(__dirname, '..', 'build')));
// Always return the main index.html, so react-router render the route in the client
app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'build', 'index.html'));
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/hoteis', (req, res) =>{
    res.json([
      {hotel: hoteis},
      {disp: disponibilidade}
    ]);
});

var content;

function constructHoteis(hoteis, callback) {
    objHotel=[];
    content.forEach(function(data){
      objTemp = data.toString().split(",");
      objHotel.push({key:objTemp[0], text:objTemp[2], value:objTemp[1]});
      hoteis = objHotel;
    })
}

module.exports = app;
